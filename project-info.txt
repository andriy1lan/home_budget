This HomeBudget (Personal Purse) project
created as Standart Java Project in Eclipse.
Then converted and refactored for Eclipse
Maven Project.
Works as with jdk(jre) 7 and tomcat server 7.0
as well as with jdk(jre) 8 and tomcat server 8.0
It stores the data in MS SQL SERVER db - with dump
provided.
To run it, you need add manually 
sqljdbc4 4.0 dependency jar and pom
to local m2 maven repository.

Enter localhost:8080/HomeBudget/input
to use the app in browser after server
succesfully started.

Technologies/Tools: Spring MVC, JDBC, MS SQL Server, JSP, JSTL