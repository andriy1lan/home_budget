<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title> Personalnyi Hamanets </title>
    <style>
    .error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
    </style>
</head>
<body>
<div style="position:absolute; left:120px; background-color:grey;">
<h2>Dodaty dohody</h2>
<form:form method="POST" action="/HomeBudget/addInput">
   <form:label path="sumadoh">Suma dohodiv</form:label>
   <form:input path="sumadoh"/>
   <form:errors path="sumadoh" cssClass="error" element="div"/>
   <br>
   <br>
   <form:label path="">Vvedit datu v formati yyyy-mm-dd</form:label>
   <form:input path="ddate"/>
   <form:errors path="ddate" element="div"/>
   <br>
   <fieldset>
   <legend>
   Vyberit kategoriyu dohodiv
   </legend> 
   <form:radiobutton path="doh" value="Zarplata"/>Zarplata
   <form:radiobutton path="doh" value="Pozyky"/>Pozyky
   <form:radiobutton path="doh" value="ProcDeposytiv"/>ProcDeposytiv
   <form:radiobutton path="doh" value="TymchasDohody"/>TymchasDohody
   <br>
   <form:radiobutton path="doh" value="Podarunky"/>Podarunky
   <form:radiobutton path="doh" value="Prodazhi"/>Prodazhi
   <form:radiobutton path="doh" value="DopomogBezrobittyu"/>DopomogBezrobittyu
   <form:radiobutton path="doh" value="Inshe"/>Inshe
   </fieldset>
   <form:errors path="doh" cssClass="error" element="div"/>
   <form:label path="">${status}</form:label>
   <br>
   <form:button>Dodaty dohody</form:button>
</form:form>
</div>

<div style="position:absolute; left:570px; background-color:grey;">
<h2>Dodaty Vytraty</h2>
<form:form method="POST" action="/HomeBudget/addOutput">
   <form:label path="">Suma vytrat</form:label>
   <form:input path="sumavyt"/>
   <br>
   <br>
   <form:label path="">Vvedit datu v formati yyyy-mm-dd</form:label>
   <form:input path="vdate"/>
   <br>
   <fieldset>
   <legend>
   Vyberit kategoriyu vytrat
   </legend>
   <form:radiobutton path="vyt" value="Produkty"/>Produkty
   <form:radiobutton path="vyt" value="PobutTechnika"/>PobutTechnika
   <form:radiobutton path="vyt" value="RechiOdyag"/>RechiOdyag
   <form:radiobutton path="vyt" value="Mayno"/>Mayno
   <br>
   <form:radiobutton path="vyt" value="KomunVytraty"/>KomunVytraty
   <form:radiobutton path="vyt" value="KyshenkoviVytraty"/>KyshenkoviVytraty
   <form:radiobutton path="vyt" value="Zaoschadzhennya"/>Zaoschadzhennya
   <br>
   <form:radiobutton path="vyt" value="ProcentCredytiv"/>ProcentCredytiv
   <form:radiobutton path="vyt" value="Strahuvannya"/>Strahuvannya
   <form:radiobutton path="vyt" value="Inshe"/>Inshe
   </fieldset>
   <form:errors path="vyt" cssClass="error" element="div"/>
   <form:label path="">${vstatus}</form:label>
   <br>
   <form:button>Dodaty vytraty</form:button>
</form:form>
</div>

<div style="position:absolute; top:290px; left:120px;">
<h2>Pereviryty dohody</h2>
<form:form method="POST" action="/HomeBudget/selectInput">
   <form:label path="">Vvedit pochahtkovu datu</form:label>
   <form:input path="ddate1"/>
   <form:errors path="ddate1" element="div"/>
   <br>
   <form:label path="">Vvedit kintsevu datu</form:label>
   <form:input path="ddate2"/>
   <form:errors path="ddate2" element="div"/>
   <br>
   <fieldset>
   <legend>
   Vyberit dohody
   </legend>
   <form:checkbox path="dohody" value="Zarplata"/>Zarplata
   <form:checkbox path="dohody" value="Pozyky"/>Pozyky
   <form:checkbox path="dohody" value="ProcDeposytiv"/>ProcDeposytiv
   <form:checkbox path="dohody" value="TymchasDohody"/>TymchasDohody
   <br>
   <form:checkbox path="dohody" value="Podarunky"/>Podarunky
   <form:checkbox path="dohody" value="Prodazhi"/>Prodazhi
   <form:checkbox path="dohody" value="DopomogBezrobittyu"/>DopomogBezrobittyu
   <form:checkbox path="dohody" value="Inshe"/>Inshe
   </fieldset>
   <form:errors path="dohody" cssClass="error" element="div"/>
   <form:label path=""> ${status1} </form:label>
   <br>
   <form:button>Vyznachte dohody</form:button>
   
 <h3>Dohody za period: ${ddate1} -- ${ddate2}</h3>
 <table style="background-color:grey; border="1">
 <c:forEach var="namee" items="${dohody1}">
 <th>${namee}</th>
 </c:forEach>
 <tr>
 <c:forEach var="data" items="${list}">
 <td>${data}</td>
 </c:forEach>
 </tr>
 </table>
 
</form:form>
</div>
 
 
 <div style="position:absolute; top:290px; left:570px;">
<h2>Pereviryty vytraty</h2>
<form:form method="POST" action="/HomeBudget/selectOutput">
   <form:label path="">Vvedit pochahtkovu datu</form:label>
   <form:input path="vdate1"/>
   <form:errors path="vdate1" element="div"/>
   <br>
   <form:label path="">Vvedit kintsevu datu</form:label>
   <form:input path="vdate2"/>
   <form:errors path="vdate2" element="div"/>
   <br>
   <fieldset>
   <legend>
   Vyberit vytraty
   </legend>
   <form:checkbox path="vytraty" value="Produkty"/>Produkty
   <form:checkbox path="vytraty" value="PobutTechnika"/>PobutTechnika
   <form:checkbox path="vytraty" value="RechiOdyag"/>RechiOdyag
   <form:checkbox path="vytraty" value="Mayno"/>Mayno
   <br>
   <form:checkbox path="vytraty" value="KomunVytraty"/>KomunVytraty
   <form:checkbox path="vytraty" value="KyshenkoviVytraty"/>KyshenkoviVytraty
   <form:checkbox path="vytraty" value="Zaoschadzhennya"/>Zaoschadzhennya
   <br>
   <form:checkbox path="vytraty" value="ProcentCredytiv"/>ProcentCredytiv
   <form:checkbox path="vytraty" value="Strahuvannya"/>Strahuvannya
   <form:checkbox path="vytraty" value="Inshe"/>Inshe
   </fieldset>
   <form:errors path="vytraty" cssClass="error" element="div"/>
   <form:label path=""> ${vstatus1} </form:label>
   <br>
   <form:button>Vyznachte vytraty</form:button>
   
 <h3>Vytraty za period: ${vdate1} -- ${vdate2}</h3>
 <table style="background-color:grey;" border="1"> 
 <c:forEach var="namee" items="${vytraty1}">
 <th>${namee}</th>
 </c:forEach>
 <tr>
 <c:forEach var="data" items="${vlist}">
 <td>${data}</td>
 </c:forEach>
 </tr>
 </table>
 
</form:form>
</div>

 
</body>
</html>