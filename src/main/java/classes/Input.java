package classes;

public class Input {
	  
	  // @NotNull (message="Zapovnit pole dohodiv")
	  // @NumberFormat(style = Style.NUMBER, pattern = "##.###")
	   private Integer sumadoh;
	   
	    
	   private Integer sumavyt;
	   
	  // @NotEmpty (message="Vyberit kategoriyu dohodiv")
	   private String doh;
	   
	   private String vyt;
	   
	   
	  // @Size(min=10, max=10)
	 //  @DateTimeFormat(pattern="dd.MM.YYYY")
	   private String ddate;
	   
	   //@Size(min=10, max=10)
	   //@DateTimeFormat(pattern="dd.MM.YYYY")
	   private String ddate1;
	   
	   private String ddate2;
	   
	   private String vdate;
	   
	   private String vdate1;
	   
	   private String vdate2;
	   
	   private String status;
	   
	  // @NotNull (message="Vyberit kategoriyi dohodiv")
	   private String [] dohody;
	   
	   private String [] vytraty;
	   
	   
	   public void setDoh(String doh) {
		      this.doh = doh;
		   }
	   
	   public String getDoh () {
		   return doh;
	   }
	   
	   public void setDdate(String ddate) {
		      this.ddate=ddate;
		   }
	   
	   public String getDdate () {
		   return ddate;
	   }
	   
	   public void setVdate(String vdate) {
		      this.vdate = vdate;
		   }
	   
	   public String getVdate () {
		   return vdate;
	   }
	   
	   public void setDdate1(String ddate1) {
		      this.ddate1 = ddate1;
		   }
	   
	   public String getDdate1 () {
		   return ddate1;
	   }
	   
	   
	   public void setDdate2(String ddate2) {
		      this.ddate2 = ddate2;
		   }
	   
	   public String getDdate2 () {
		   return ddate2;
	   }
	   
	   
	   public void setVdate1(String vdate1) {
		      this.vdate1 = vdate1;
		   }
	   
	   public String getVdate1 () {
		   return vdate1;
	   }
	   
	   
	   public void setVdate2(String vdate2) {
		      this.vdate2 = vdate2;
		   }
	   
	   public String getVdate2 () {
		   return vdate2;
	   }
	   
	   
	   
	   public void setVyt(String vyt) {
			      this.vyt = vyt;
			   }
		public String getVyt() {
			      return vyt;
			   }

		
	   public void setStatus(String status) {
	      this.status= status;
	   }
	   public String getStatus() {
	      return status;
	   }
 
	   public void setSumadoh (Integer sumadoh) {
		this.sumadoh = sumadoh;   
	   }
	   
	   public Integer getSumadoh () {
		return sumadoh;   
	   }
	   
	   public void setSumavyt (Integer sumavyt) {
			this.sumavyt = sumavyt;   
		   }
	   
	   public Integer getSumavyt () {
			return sumavyt;   
		   }
	   
	   public void setDohody (String [] dohody) {
			this.dohody = dohody;   
		   }
	   
	   public String [] getDohody () {
			return dohody;   
		   }
	 
	   public void setVytraty (String [] vytraty) {
			this.vytraty = vytraty;   
		   }
	   
	   public String [] getVytraty () {
			return vytraty;   
		   }
	   
}