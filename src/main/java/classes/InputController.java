package classes;

import org.springframework.stereotype.Controller;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.beans.factory.annotation.Autowired;    
import classes.Input;
import classes.InputDao;


@Controller
public class InputController {

   @Autowired  
   InputDao dao;//will inject dao from xml file       
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */
	
   @RequestMapping(value = "/input", method = RequestMethod.GET)
   public ModelAndView input() {
      return new ModelAndView("input", "command", new Input());
   }
   
   @RequestMapping(value = "/addInput", method = RequestMethod.POST)
   public String addInput( @Valid @ModelAttribute("command")  Input input,
   BindingResult result,
   ModelMap model) {

      if (result.hasErrors()) {
			return "input";
		} else {
			try {
			if (input.getDdate().isEmpty()||input.getDoh()==null) { 
		    input.setStatus("Put correct date or choose profit");
			model.addAttribute("status", input.getStatus());
			return "input";
			}
			
			dao.save(input);
			input.setStatus("Dohody dodani");
			
			model.addAttribute("status", input.getStatus());
		    }
			catch (Exception e) {
		    input.setStatus(e.toString());
			model.addAttribute("status", input.getStatus());
			}
			return "input";
		}
      
   }
   
   @RequestMapping(value = "/selectInput", method = RequestMethod.POST)
   public String selectInput( @Valid @ModelAttribute("command")  Input input,
   BindingResult result,
   ModelMap model) {
	   
      if (result.hasErrors()) {
			return "input";
		} else {
			try {
			
			if (input.getDdate1().isEmpty()&&input.getDdate2().isEmpty()) { 
			    input.setStatus("Put correct dates");
				model.addAttribute("status1", input.getStatus());
				return "input";
				}
			
			dao.select(input);
			model.addAttribute("list", dao.list);

			model.addAttribute("status", input.getStatus());
			model.addAttribute("ddate1", input.getDdate1());
			model.addAttribute("ddate2", input.getDdate2());
			model.addAttribute("dohody1", dao.dohlist);
		   }
			catch (Exception e) {
		    input.setStatus(e.toString());
			model.addAttribute("status1", input.getStatus());
			}
			return "input";
		}
   }
   
   
   @RequestMapping(value = "/addOutput", method = RequestMethod.POST)
   public String addOutput( @Valid @ModelAttribute("command")  Input input,
   BindingResult result,
   ModelMap model) {
	   
      if (result.hasErrors()) {
			return "input";
		} else {
			try {
			
			if (input.getVdate().isEmpty()||input.getVyt().isEmpty()) { 
			    input.setStatus("Put correct date or choose expense");
				model.addAttribute("vstatus", input.getStatus());
				return "input";
				}
			
			dao.vsave(input);
			input.setStatus("Vytraty dodani");
			model.addAttribute("vstatus", input.getStatus());
		    }
			catch (Exception e) {
		    input.setStatus(e.toString());
			model.addAttribute("vstatus", input.getStatus());
			}
			return "input";
		}
      
   }
   
   @RequestMapping(value = "/selectOutput", method = RequestMethod.POST)
   public String selectOutput( @Valid @ModelAttribute("command")  Input input,
   BindingResult result,
   ModelMap model) {

      if (result.hasErrors()) {
			return "input";
		} else {
			try {
			
			if (input.getVdate1().isEmpty()&&input.getVdate2().isEmpty()) { 
			    input.setStatus("Put correct dates");
				model.addAttribute("vstatus1", input.getStatus());
				return "input";
				}
			
			dao.vselect(input);
			model.addAttribute("vlist", dao.vlist);

			model.addAttribute("status", input.getStatus());
			model.addAttribute("vdate1", input.getVdate1());
			model.addAttribute("vdate2", input.getVdate2());
			model.addAttribute("vytraty1", dao.vytlist);
		    }
			catch (Exception e) {
		    input.setStatus(e.toString());
			model.addAttribute("vstatus1", input.getStatus());
			}
			return "input";
		}
   }
   
   
   
}