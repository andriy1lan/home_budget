package classes;


import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.Date;
import classes.Input;
  
public class InputDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}

List <Integer> list;
List <String> dohlist;

List <Integer> vlist;
List <String> vytlist;

public int save (Input p) {
	
	if (p.getDdate().isEmpty()||p.getDoh().isEmpty()) return 1;
	    Date dd=Date.valueOf(p.getDdate());
	    // LocalDate dd=LocalDate.parse(p.getDdate());
	              /*        java.util.Date d=new java.util.Date();
	                      Timestamp t=null;
	                      Date dd=null;
	                      try {
		DateFormat df=new SimpleDateFormat("YYYY-MM-dd");
		 d=df.parse(p.getDdate());
		 t=new Timestamp(d.getTime());
		 dd=new Date(t.getTime());
                           }
		//d=new Date(df.parse(p.getDdate())); }
		catch (ParseException e) {p.setStatus(e.toString());} */
	    String sql="USE Budget INSERT INTO Profit("+p.getDoh()+", Date) VALUES (?,?)"; 
        return template.update(sql,p.getSumadoh(),dd);
                               
            }

public void select (Input p) { //Date d=new Date();
	list=new ArrayList <Integer>();
	
	if (p.getDdate1().isEmpty()&&p.getDdate2().isEmpty()) return;
	
	
	Date dd1=Date.valueOf(p.getDdate1());
    Date dd2=Date.valueOf(p.getDdate2());
    
	

    dohlist=Arrays.asList(p.getDohody());
    int razom=0;
    for (String item : dohlist) {
    String sql="USE Budget SELECT SUM("+item+") FROM Profit WHERE "+item+" IS NOT NULL AND " +
        	"Date >= ? AND Date <= ?";
    int sum;
    sum=template.queryForInt(sql,dd1,dd2);
    list.add(sum);
    razom+=sum;
                                }
    List<String> dohlist1=new ArrayList <String> (dohlist); 
    dohlist1.add("Razom");
    dohlist=dohlist1;
    list.add(razom);    
        }

public int vsave (Input p) { //Date d=new Date();
	if (p.getVdate().isEmpty()||p.getVyt().isEmpty()) return 1;
    Date dd=Date.valueOf(p.getVdate());
    
    String sql="USE Budget INSERT INTO Expense("+p.getVyt()+", Date) VALUES (?,?)"; 
//p.setStatus(dd.toString()); SELECT CAST(@date1 AS DATETIME) "DECLARE @date1 date="+dd+ VALUES ("+p.getSumadoh()+",'"+dd+"')"; 

   return template.update(sql,p.getSumavyt(),dd);
        }

public void vselect (Input p) { //Date d=new Date();
	vlist=new ArrayList <Integer>();
	
	if (p.getVdate1().isEmpty()&&p.getVdate2().isEmpty()) return;
	
	
	Date dd1=Date.valueOf(p.getVdate1());
    Date dd2=Date.valueOf(p.getVdate2());
    
	

    vytlist=Arrays.asList(p.getVytraty());
    int razom=0;
    for (String item : vytlist) {
    String sql="USE Budget SELECT SUM("+item+") FROM Expense WHERE "+item+" IS NOT NULL AND " +
        	"Date >= ? AND Date <= ?";
    int sum;
    sum=template.queryForInt(sql,dd1,dd2);
    vlist.add(sum);
    razom+=sum;
                                }
    List<String> vytlist1=new ArrayList <String> (vytlist); 
    vytlist1.add("Razom");
    vytlist=vytlist1;
    vlist.add(razom);    
        }


} 
